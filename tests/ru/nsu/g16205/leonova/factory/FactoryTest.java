package ru.nsu.g16205.leonova.factory;

import org.junit.Before;
import org.junit.Test;
import ru.nsu.g16205.leonova.Data;
import ru.nsu.g16205.leonova.commands.Command;

import java.io.IOException;

import static org.junit.Assert.*;

public class FactoryTest {

    Data data = new Data();
    Factory factory;

    @Before
    public void setUp() throws Exception {
        try{
            factory = new Factory(data);
        }
        catch(IOException ex){
            System.err.println(ex.getLocalizedMessage());
            ex.printStackTrace();
            return;
        }

    }

    @Test (expected = IllegalArgumentException.class)
    public void badCommand(){
        String command = "olo";
        String[] args = {"trolol"};
        try{
                factory.createCommandInstance(command, args);
            }
        catch(IllegalAccessException | ClassNotFoundException | InstantiationException ex) {
            System.err.println(ex.getLocalizedMessage());
            ex.printStackTrace();
            return;
        }
    }

    @Test
    public void pushCreated(){
        String command = "PUSH";
        String[] args = {"100"};
        Command cmd;
        try{
            cmd = factory.createCommandInstance(command, args);
        }
        catch(IllegalAccessException | ClassNotFoundException | InstantiationException ex) {
            System.err.println(ex.getLocalizedMessage());
            ex.printStackTrace();
            return;
        }
        cmd.execute();
        assertEquals(100.0, data.getStack().peek(), 0.001);

    }

    @Test
    public void popCreated(){
        String command = "POP";
        String[] args = {""};
        Command cmd;
        try{
            cmd = factory.createCommandInstance(command, args);
        }
        catch(IllegalAccessException | ClassNotFoundException | InstantiationException ex) {
            System.err.println(ex.getLocalizedMessage());
            ex.printStackTrace();
            return;
        }
        data.getStack().push(0.0);
        data.getStack().push(1.0);
        cmd.execute();
        assertEquals(0.0, data.getStack().peek(), 0.001);

    }

    @Test
    public void addCreated(){
        String command = "+";
        String[] args = {""};
        Command cmd;
        try{
            cmd = factory.createCommandInstance(command, args);
        }
        catch(IllegalAccessException | ClassNotFoundException | InstantiationException ex) {
            System.err.println(ex.getLocalizedMessage());
            ex.printStackTrace();
            return;
        }
        data.getStack().push(100.0);
        data.getStack().push(1.0);
        cmd.execute();
        assertEquals(101.0, data.getStack().peek(), 0.001);

    }

    @Test
    public void subCreated(){
        String command = "-";
        String[] args = {""};
        Command cmd;
        try{
            cmd = factory.createCommandInstance(command, args);
        }
        catch(IllegalAccessException | ClassNotFoundException | InstantiationException ex) {
            System.err.println(ex.getLocalizedMessage());
            ex.printStackTrace();
            return;
        }
        data.getStack().push(1.0);
        data.getStack().push(101.0);
        cmd.execute();
        assertEquals(100.0, data.getStack().peek(), 0.001);
    }

    @Test
    public void divCreated(){
        String command = "/";
        String[] args = {""};
        Command cmd;
        try{
            cmd = factory.createCommandInstance(command, args);
        }
        catch(IllegalAccessException | ClassNotFoundException | InstantiationException ex) {
            System.err.println(ex.getLocalizedMessage());
            ex.printStackTrace();
            return;
        }
        data.getStack().push(2.0);
        data.getStack().push(100.0);
        cmd.execute();
        assertEquals(50.0, data.getStack().peek(), 0.001);

    }

    @Test
    public void mulCreated(){
        String command = "*";
        String[] args = {""};
        Command cmd;
        try{
            cmd = factory.createCommandInstance(command, args);
        }
        catch(IllegalAccessException | ClassNotFoundException | InstantiationException ex) {
            System.err.println(ex.getLocalizedMessage());
            ex.printStackTrace();
            return;
        }
        data.getStack().push(10.0);
        data.getStack().push(11.0);
        cmd.execute();
        assertEquals(110.0, data.getStack().peek(), 0.001);

    }

    @Test
    public void sqrtCreated(){
        String command = "SQRT";
        String[] args = {""};
        Command cmd;
        try{
            cmd = factory.createCommandInstance(command, args);
        }
        catch(IllegalAccessException | ClassNotFoundException | InstantiationException ex) {
            System.err.println(ex.getLocalizedMessage());
            ex.printStackTrace();
            return;
        }
        data.getStack().push(100.0);
        cmd.execute();
        assertEquals(10.0, data.getStack().peek(), 0.001);
    }

    @Test
    public void defineCreated(){
        String command = "DEFINE";
        String[] args = {"a", "100.0"};
        Command cmd;
        try{
            cmd = factory.createCommandInstance(command, args);
        }
        catch(IllegalAccessException | ClassNotFoundException | InstantiationException ex) {
            System.err.println(ex.getLocalizedMessage());
            ex.printStackTrace();
            return;
        }
        cmd.execute();
        assertEquals(100.0, data.getMap().get("a"), 0.001);

    }


}