package ru.nsu.g16205.leonova.commands;

import org.junit.Before;
import org.junit.Test;
import ru.nsu.g16205.leonova.Data;

import static org.junit.Assert.*;

public class PushTest {

    Data data = new Data();
    Push push = new Push();

    @Before
    public void setUp(){
        push.setData(data);
        data.getMap().put("ololo", 50.123);
        data.getMap().put("azaza", -123.132);
    }
    @Test (expected = IllegalArgumentException.class)
    public void putLessArgs() {
        String[] str = {""};
        push.putArgs(str);
    }

    @Test (expected = IllegalArgumentException.class)
    public void putMoreArgs() {
        String[] str = {"ololo", "trololo"};
        push.putArgs(str);
    }

    @Test (expected = IllegalArgumentException.class)
    public void putBadSymbolsArgs() {
        String[] str = {"Bl@"};
        push.putArgs(str);
    }

    @Test (expected = IllegalArgumentException.class)
    public void putNotNumberNotVariableArgs() {
        String[] str = {"123f44aa"};
        push.putArgs(str);
    }

    @Test
    public void executeVariable() {
        String[] str = {"ololo"};
        push.putArgs(str);
        push.execute();
        assertEquals(50.123, data.getStack().peek(), 0.001);
    }

    @Test
    public void executeValue() {
        String[] str = {"123.0"};
        push.putArgs(str);
        push.execute();
        assertEquals(123.0, data.getStack().peek(), 0.001);
    }

}