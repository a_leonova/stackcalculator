package ru.nsu.g16205.leonova.commands;

import org.junit.Before;
import org.junit.Test;
import ru.nsu.g16205.leonova.Data;

import static org.junit.Assert.*;

public class DivTest {
    Data data = new Data();
    Div div = new Div();

    @Before
    public void setUp() throws Exception {
        div.setData(data);
        data.getStack().push(-2.0);
        data.getStack().push(5.0);
        data.getStack().push(-10.0);
        data.getStack().push(100.0);

    }

    @Test (expected = IllegalArgumentException.class)
    public void putArgs() {
        String[] str = {"ololo"};
        div.putArgs(str);
    }

    @Test
    public void execute() {
        div.execute();//100 / -10
        assertEquals(-10, data.getStack().peek(), 0.001);
        div.execute(); //100 / -10 / 5
        assertEquals(-2, data.getStack().peek(), 0.001);
        div.execute(); //100 / -10 / 5 / -2
        assertEquals(1, data.getStack().peek(), 0.001);
    }

    @Test (expected = ArithmeticException.class)
    public void executeWithThrows(){
        data.getStack().push(0.0);
        data.getStack().push(1.0);
        div.execute();
    }
}