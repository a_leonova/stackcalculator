package ru.nsu.g16205.leonova.commands;

import org.junit.Before;
import org.junit.Test;
import ru.nsu.g16205.leonova.Data;

import java.io.ByteArrayOutputStream;

import static org.junit.Assert.*;

public class PrintTest {
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    Data data = new Data();
    Print print = new Print(output);

    @Before
    public void setUp() {
        print.setData(data);
        data.getStack().push(123.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void putTwoArgs() {
        String[] str = {"ololo", "trololo"};
        print.putArgs(str);
    }

    @Test
    public void execute() {
        print.execute();
        String expected = String.valueOf(123.0) + System.getProperty("line.separator");
        String actual = output.toString();
        assertEquals(expected, actual);
    }


}