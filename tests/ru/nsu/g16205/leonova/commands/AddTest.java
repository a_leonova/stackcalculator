package ru.nsu.g16205.leonova.commands;

import jdk.management.resource.internal.inst.DatagramDispatcherRMHooks;
import org.junit.Before;
import org.junit.Test;
import ru.nsu.g16205.leonova.Data;
import ru.nsu.g16205.leonova.commands.Add;

import static org.junit.Assert.*;

public class AddTest {
    Add add = new Add();
    Data data = new Data();

    @Before
    public void setUp() {
        data.getStack().push(-137.48);
        data.getStack().push(1.12);
        data.getStack().push(32.18);
        data.getStack().push(4.00);
        data.getStack().push(100.18);
        add.setData(data);
    }

    @Test (expected = IllegalArgumentException.class)
    public void putTwoArgs() {
        String[] str = {"ololo", "trololo"};
        add.putArgs(str);
    }

    @Test
    public void execute() {
        add.execute(); //100.18 + 4.00
        assertEquals(104.18, data.getStack().peek(), 0.001);
        add.execute(); //100.18+4.00+32.18
        assertEquals(136.36, data.getStack().peek(), 0.001);
        add.execute(); //100.18+4.00+32.18 + 1.12
        assertEquals(137.48, data.getStack().peek(), 0.001);
        add.execute(); //100.18+4.00+32.18 + 1.12 - 137.48
        assertEquals(0.00, data.getStack().peek(), 0.001);
    }

}