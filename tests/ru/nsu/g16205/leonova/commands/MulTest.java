package ru.nsu.g16205.leonova.commands;

import org.junit.Before;
import org.junit.Test;
import ru.nsu.g16205.leonova.Data;

import static org.junit.Assert.*;

public class MulTest {
    Data data = new Data();
    Mul mul = new Mul();

    @Before
    public void setUp() throws Exception {
        mul.setData(data);
        data.getStack().push(0.0);
        data.getStack().push(-0.1);
        data.getStack().push(-0.5);
        data.getStack().push(100.0);
        data.getStack().push(1.0);

    }

    @Test(expected = IllegalArgumentException.class)
    public void putArgs() {
        String[] str = {"ololo"};
        mul.putArgs(str);
    }

    @Test
    public void execute() {
        mul.execute(); //1.0 * 100.0
        assertEquals(100, data.getStack().peek(), 0.001);
        mul.execute(); //1.0*100.0*-0.5
        assertEquals(-50, data.getStack().peek(), 0.001);
        mul.execute(); //1.0 * 100.0 * -0.5 * -0.1
        assertEquals(5, data.getStack().peek(), 0.001);
        mul.execute(); //1.0 * 100.0 * -0.5 * -0.1 * 0.0
        assertEquals(0, data.getStack().peek(), 0.001);
    }

}