package ru.nsu.g16205.leonova.commands;

import org.junit.Before;
import org.junit.Test;
import ru.nsu.g16205.leonova.Data;

import static org.junit.Assert.*;

public class SqrtTest {
    Sqrt sqrt = new Sqrt();
    Data data = new Data();

    @Before
    public void setUp() {
        data.getStack().push( 1024.0);
        sqrt.setData(data);
    }

    @Test(expected = IllegalArgumentException.class)
    public void putTwoArgs() {
        String[] str = {"ololo", "trololo"};
        sqrt.putArgs(str);
    }

    @Test
    public void execute() {
        sqrt.execute(); //sqrt(1024)
        assertEquals(32.0,data.getStack().peek(), 0.001);
        data.getStack().push(0.0);
        sqrt.execute();
        assertEquals(0.0, data.getStack().peek(), 0.001);
    }

    @Test (expected = ArithmeticException.class)
    public void NegativeExecute(){
        data.getStack().push(-100.0);
        sqrt.execute();
    }


}