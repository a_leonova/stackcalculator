package ru.nsu.g16205.leonova.commands;

import org.junit.Before;
import org.junit.Test;
import ru.nsu.g16205.leonova.Data;

import static org.junit.Assert.*;

public class DefineTest {
    Define define = new Define();
    Data data = new Data();

    @Before
    public void setUp() throws Exception {
        define.setData(data);
    }

    @Test (expected = IllegalArgumentException.class)
    public void putOneArg() {
        String []args = {"a"};
        define.putArgs(args);
    }

    @Test (expected = IllegalArgumentException.class)
    public void putBadDefineString(){
        String[] args = {"1234", "1.2"};
        define.putArgs(args);
    }

    @Test (expected = IllegalArgumentException.class)
    public void putBadNumber(){
        String[] args = {"Ololo", "12csa"};
        define.putArgs(args);
    }

    @Test
    public void execute() {
        String[] args = {"a", "123"};
        define.putArgs(args);
        define.execute();
        Double value = data.getMap().get("a");
        assertEquals(123.0, value, 0.1);
    }


}