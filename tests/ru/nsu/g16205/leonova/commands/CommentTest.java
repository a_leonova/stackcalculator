package ru.nsu.g16205.leonova.commands;

import org.junit.Test;

import static org.junit.Assert.*;

public class CommentTest {
    Comment comment = new Comment();

    @Test
    public void putArgs() {
        String[] args = {"Hello", "world", "!"};
        comment.putArgs(args);
        String value = "Hello world ! ";
        assertEquals(value, comment.getComment());
    }
}