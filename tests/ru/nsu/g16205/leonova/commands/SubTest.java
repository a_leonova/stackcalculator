package ru.nsu.g16205.leonova.commands;

import org.junit.Before;
import org.junit.Test;
import ru.nsu.g16205.leonova.Data;

import static org.junit.Assert.*;

public class SubTest {
    Data data = new Data();
    Sub sub = new Sub();

    @Before
    public void setUp() throws Exception {
        sub.setData(data);
        data.getStack().push(-40.0);
        data.getStack().push(90.0);
        data.getStack().push(50.0);
        data.getStack().push(10.0);
        data.getStack().push(110.0);

    }

    @Test (expected = IllegalArgumentException.class)
    public void putArgs() {
        String[] str = {"ololo"};
        sub.putArgs(str);
    }

    @Test
    public void execute() {
        sub.execute(); //110 - 10
        assertEquals(100, data.getStack().peek(), 0.001);
        sub.execute(); //110 - 10 - 50
        assertEquals(50, data.getStack().peek(), 0.001);
        sub.execute(); //110 - 10 - 50 - 90
        assertEquals(-40, data.getStack().peek(), 0.001);
        sub.execute(); //110 - 10 - 50 - 90 -(-40)
        assertEquals(0, data.getStack().peek(), 0.001);
    }
}