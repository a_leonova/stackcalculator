package ru.nsu.g16205.leonova;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Data {

    Stack<Double> stack;
    Map<String, Double> map;

    public Data(){
        stack = new Stack<>();
        map = new HashMap<>();
    }

    public Stack<Double> getStack(){
        return stack;
    }

    public Map<String, Double> getMap(){
        return map;
    }

}
