package ru.nsu.g16205.leonova.commands;

import org.apache.log4j.Logger;
import ru.nsu.g16205.leonova.Data;

public class Comment implements Command {

    static Logger log = Logger.getLogger(Comment.class.getName());
    String comment = "";
    Data data;

    public Comment(){
    }

    @Override
    public void execute() { }

    @Override
    public void putArgs(String[] args) {

        for (int i = 0; i < args.length; ++i){
            comment += (args[i] + " ");
        }
        log.trace("Comment.putArgs: " + comment);
    }

    @Override
    public void setData(Data otherData) {
        data = otherData;
    }

    public String getComment() {
        return comment;
    }
}
