package ru.nsu.g16205.leonova.commands;

import org.apache.log4j.Logger;
import ru.nsu.g16205.leonova.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.Stack;

public class Pop implements Command{

    Data data;
    static Logger log = Logger.getLogger(Pop.class.getName());

    public Pop(){
    }

    @Override
    public void setData(Data otherData) {
        this.data = otherData;
    }

    @Override
    public void execute() throws EmptyStackException{
        data.getStack().pop();
    }

    @Override
    public void putArgs(String[] args) throws IllegalArgumentException{
        ArrayList<String> str = new ArrayList<>(Arrays.asList(args));
        log.trace("Pop.putArgs" + str);

        if (args[0].length() != 0){
            log.error("Pop.putArgs: bad number of args - " + str);
            throw new IllegalArgumentException("Pop doesn't need arguments");
        }
    }
}
