package ru.nsu.g16205.leonova.commands;

import org.apache.log4j.Logger;
import ru.nsu.g16205.leonova.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EmptyStackException;

public class Add implements Command{
    static Logger log = Logger.getLogger(Add.class.getName());
    Data data;

    public Add(){    }

    @Override
    public void putArgs(String[] args) throws IllegalArgumentException {

        ArrayList<String> str = new ArrayList<>(Arrays.asList(args));
        log.trace("Add.putArgs" + str);

        if (args[0].length() != 0){
            log.error("args in Add.putArgs is not empty: " + str);
            throw  new IllegalArgumentException("Addition doesn't need argument");
        }
    }

    @Override
    public void execute() throws EmptyStackException{
        Double a = data.getStack().pop();
        Double b = data.getStack().pop();
        log.trace("Add.execute" + a + " " + b);
        data.getStack().push(a + b);
    }

    @Override
    public void setData(Data otherData) {
        this.data = otherData;
    }
}
