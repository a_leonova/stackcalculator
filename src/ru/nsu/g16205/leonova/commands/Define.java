package ru.nsu.g16205.leonova.commands;

import org.apache.log4j.Logger;
import ru.nsu.g16205.leonova.Data;

import java.util.ArrayList;
import java.util.Arrays;

public class Define implements Command {

    Data data;
    String str;
    Double value;
    static Logger log = Logger.getLogger(Define.class.getName());

    @Override
    public void execute() {
        log.trace("Define.execute: " + str + " " + value);
        data.getMap().put(str, value);
    }

    @Override
    public void putArgs(String[] args) throws IllegalArgumentException {

        ArrayList<String> string = new ArrayList<>(Arrays.asList(args));
        log.trace("Define.putArgs" + string);

        if (args.length != 2){
            log.error("Define.putArgs: args are not 2 - " + string);
            throw new IllegalArgumentException("Define needs 2 arguments");
        }

        value = checkDouble(args[1]);
        if (!isCorrectString(args[0])){
            log.error("Define.putArgs: bad string - " + args[0] + " " + args[1]);
            throw new IllegalArgumentException("String must consist of digits or letters and cannot be a number");
        }
        str = args[0];
    }

    @Override
    public void setData(Data otherData) {
        data = otherData;
    }

    private boolean isCorrectString(String str){

        boolean isLetter = false;

        char[] chars = str.toCharArray();
        for (char c : chars){
            if (!Character.isDigit(c) && !Character.isLetter(c)){
                return false;
            }
            if (Character.isLetter(c)){
                isLetter = true;
            }
        }

        if (!isLetter){
            return false;
        }

        return true;
    }
    private Double checkDouble(String str) throws NumberFormatException{
        return Double.valueOf(str);
    }
}
