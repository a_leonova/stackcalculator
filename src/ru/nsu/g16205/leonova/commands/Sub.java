package ru.nsu.g16205.leonova.commands;

import org.apache.log4j.Logger;
import ru.nsu.g16205.leonova.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EmptyStackException;

public class Sub implements Command{
    Data data;
    static Logger log = Logger.getLogger(Sub.class.getName());

    public Sub(){    }

    @Override
    public void putArgs(String[] args) throws IllegalArgumentException {
        ArrayList<String> str = new ArrayList<>(Arrays.asList(args));
        log.trace("Sub.putArgs " + str);

        if (args[0].length() != 0){
            log.error("Sub.putArgs: bad number of args - " + str);
            throw  new IllegalArgumentException("Sub doesn't need argument");
        }
    }

    @Override
    public void execute() throws EmptyStackException {
        Double a = data.getStack().pop();
        Double b = data.getStack().pop();
        log.trace("Sub.execute " + a + " " + b);
        data.getStack().push(a - b);
    }

    @Override
    public void setData(Data otherData) {
        data = otherData;
    }
}
