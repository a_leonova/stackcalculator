package ru.nsu.g16205.leonova.commands;

import ru.nsu.g16205.leonova.Data;

public interface Command {
    public void execute();
    public void putArgs(String[] args);
    public void setData(Data otherData);
}
