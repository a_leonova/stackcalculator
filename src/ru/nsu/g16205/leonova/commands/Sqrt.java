package ru.nsu.g16205.leonova.commands;

import org.apache.log4j.Logger;
import ru.nsu.g16205.leonova.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EmptyStackException;

public class Sqrt implements Command {
    Data data;
    static Logger log = Logger.getLogger(Sqrt.class.getName());

    public Sqrt(){    }

    @Override
    public void putArgs(String[] args) throws IllegalArgumentException {
        ArrayList<String> str = new ArrayList<>(Arrays.asList(args));

        log.trace("Sqrt.putArgs" + str);

        if (args[0].length() != 0){
            log.error("Sqrt.putArgs: bad number of args - " + str);
            throw  new IllegalArgumentException("Sqrt doesn't need argument");
        }
    }

    @Override
    public void execute() throws EmptyStackException,
            ArithmeticException {
        Double a = data.getStack().pop();
        log.trace("Sqrt.execute " + a);
        if (a < 0){
            log.error("Sqrt.execute: number < 0 - " + a);
            throw new ArithmeticException("No sqrt with negative numbers");
        }
        data.getStack().push(Math.sqrt(a));
    }

    @Override
    public void setData(Data otherData) {
        this.data = otherData;
    }
}
