package ru.nsu.g16205.leonova.commands;

import org.apache.log4j.Logger;
import ru.nsu.g16205.leonova.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EmptyStackException;

public class Div implements Command {

    Data data;
    static Logger log = Logger.getLogger(Div.class.getName());

    @Override
    public void setData(Data otherData) {
        data = otherData;
    }

    @Override
    public void putArgs(String[] args) throws IllegalArgumentException{

        ArrayList<String> str = new ArrayList<>(Arrays.asList(args));
        log.trace("Div.putArgs: " + str);

        if (args[0].length() != 0){
            log.error("Div.putArgs: bad number of args - " + str);
            throw new IllegalArgumentException("Division doesn't need arguments");
        }
    }

    @Override
    public void execute() throws EmptyStackException,
            ArithmeticException{
        Double a = data.getStack().pop();
        Double b = data.getStack().pop();
        if (b == 0.0){
            throw new ArithmeticException("Division on zero!");
        }
        data.getStack().push(a / b);
    }
}
