package ru.nsu.g16205.leonova.commands;

import org.apache.log4j.Logger;
import ru.nsu.g16205.leonova.Data;

import java.util.ArrayList;
import java.util.Arrays;

public class Push implements Command {

    Data data;
    Double valueToPush = 0.0;
    static Logger log = Logger.getLogger(Push.class.getName());

    public Push(){
    }

    @Override
    public void setData(Data otherData) {
        this.data = otherData;
    }

    @Override
    public void execute() {
        log.trace("PUSH.execute " + valueToPush);
        data.getStack().push(valueToPush);
        //TODO: what is no memory???????????
    }

    @Override
    public void putArgs(String[] args) throws IllegalArgumentException{

        ArrayList<String> str = new ArrayList<>(Arrays.asList(args));
        log.trace("Push.putArgs " + str);

        if (args.length != 1){
            log.error("Push.putArgs: bad number of args - " + str);
            throw new IllegalArgumentException("Command PUSH uses only 1 argument");
        }

        if (!isLettersOrDigits(args[0])){
            log.error("Push.putArgs: bad argument - " + str);
            throw new IllegalArgumentException("Argument must have only digits, letters");
        }

        Double value = data.getMap().get(args[0]);
        if (value == null){
            value = Double.valueOf(args[0]);
        }

        valueToPush = value;
    }

    private boolean isLettersOrDigits(String str){

        char[] chars = str.toCharArray();
        for (char c : chars ){
            if (!Character.isLetter(c) && !Character.isDigit(c) && c != '.' && c != '-'){
                return  false;
            }
        }
        return true;
    }
}
