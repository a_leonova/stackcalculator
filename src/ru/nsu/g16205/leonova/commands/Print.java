package ru.nsu.g16205.leonova.commands;

import ru.nsu.g16205.leonova.Data;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.EmptyStackException;

public class Print implements Command {
    Data data;
    PrintStream printStream;

    public Print(){
        printStream = System.out;
    }
    public Print(OutputStream output){
        printStream = new PrintStream(output);
    }

    @Override
    public void putArgs(String[] args) throws IllegalArgumentException {
        if (args[0].length() != 0){
            throw  new IllegalArgumentException("print doesn't need argument");
        }
    }

    @Override
    public void execute() throws EmptyStackException {
        Double a = data.getStack().peek();
        printStream.println(a);
    }

    @Override
    public void setData(Data otherData) {
        data = otherData;
    }
}
