package ru.nsu.g16205.leonova;

import org.apache.log4j.Logger;
import ru.nsu.g16205.leonova.commands.Add;
import ru.nsu.g16205.leonova.commands.Command;
import ru.nsu.g16205.leonova.factory.Factory;

import java.io.*;
import java.util.Arrays;
import java.util.EmptyStackException;


public class Main {

   private static String command;
   private static String[] args;
   private static Logger log = Logger.getLogger(Add.class.getName());


    public static void main(String[] argv){
        BufferedReader reader;
        if (argv.length == 0){
            reader = new BufferedReader(new InputStreamReader(System.in));
        }

        else if (argv.length == 2){
            try{
                reader = new BufferedReader(new FileReader(argv[1]));
            }
            catch (FileNotFoundException ex){
                System.err.println("Couldn't open " + argv[1] + "file");
                return;
            }
        }
        else{
            log.error("More args than need");
            System.err.println("Write filename (with path) or noting");
            return;
        }

        Data data = new Data();
        Factory factory;
        try{
            factory = new Factory(data);
        }
        catch (IOException ex){
            System.err.println(ex.getLocalizedMessage());
            System.err.println(Arrays.toString(ex.getStackTrace()));
            return;
        }

        String str;
        try {
            str = reader.readLine();
            while (!str.equals("STOP")) {
                parseString(str);

                Command cmd = factory.createCommandInstance(command, args);
                cmd.execute(); //no "no handler exception" ??

                str = reader.readLine();
            }
        }
        catch (ClassNotFoundException ex){
            System.err.println(ex.getLocalizedMessage());
            System.err.println(Arrays.toString(ex.getStackTrace()));
            log.error("Main: couldn't find class for command");
        }
        catch (InstantiationException ex){
            System.err.println(ex.getLocalizedMessage());
            System.err.println(Arrays.toString(ex.getStackTrace()));
            log.error("Main: couldn't create file");
        }
        catch (IllegalAccessException ex){
            System.err.println(ex.getLocalizedMessage());
            System.err.println(Arrays.toString(ex.getStackTrace()));
            log.error("Main: illegal access exception");
        }
        catch(IllegalArgumentException ex){
            System.err.println(ex.getLocalizedMessage());
            System.err.println(Arrays.toString(ex.getStackTrace()));
            log.error("Main: bad arg");
        }
        catch(EmptyStackException ex ){
            System.err.println(ex.getLocalizedMessage());
            System.err.println(Arrays.toString(ex.getStackTrace()));
            log.error("Main: empty stack");
        }
        catch(IOException ex){
            System.err.println(ex.getLocalizedMessage());
            System.err.println(Arrays.toString(ex.getStackTrace()));
        }
    }

    private static void parseString(String str){
       String[] splitedStr = str.split(" ");
       command = splitedStr[0];
       if (splitedStr.length == 1){
           args = new String[]{""};
       }
       else {
           args = Arrays.copyOfRange(splitedStr, 1, splitedStr.length);
       }
    }
}
