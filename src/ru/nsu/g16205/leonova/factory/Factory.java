package ru.nsu.g16205.leonova.factory;

import org.apache.log4j.Logger;
import ru.nsu.g16205.leonova.Data;
import ru.nsu.g16205.leonova.commands.Add;
import ru.nsu.g16205.leonova.commands.Command;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.io.InputStream;

public class Factory {

    Data data;
    Properties prop = new Properties();
    InputStream input;
    static Logger log = Logger.getLogger(Factory.class.getName());

    public Factory(Data otherData) throws IOException {
        data = otherData;

        input = Factory.class.getResourceAsStream("resource.txt");
        if (input == null){
            log.error("Factory.constructor: couldn't open file");
            throw new FileNotFoundException("Couldn't open resource file");
        }
        prop.load(input);
    }

    public Command createCommandInstance(String command, String[] args) throws IllegalArgumentException,
            ClassNotFoundException,
            InstantiationException,
            IllegalAccessException
    {
        String classPath =  prop.getProperty(command);

        if (classPath == null){
            log.error("No command: " + command);
            throw new IllegalArgumentException();
        }

        Class objClass = Class.forName(classPath);
        Command objCommand = (Command) objClass.newInstance();
        objCommand.setData(data);
        objCommand.putArgs(args);

        return objCommand;
    }
}
